def calculate_square_of_the_number(number):
    return number * number


def convert_temperature_in_celsius_to_fahrenheit(temperature_in_celsius):
    return temperature_in_celsius * 1.8 + 32


def calculate_volume_of_the_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1 Napisz funkcję która zwraca kwadrat dowolnej liczby.
    # Policz kwadraty liczb: 0, 16, 2.55
    print(calculate_square_of_the_number(0))
    print(calculate_square_of_the_number(16))
    print(calculate_square_of_the_number(2.55))

    # Zadanie 2 Napisz funkcję która konwertuje stopnie Celsjusza na Fahrenheity.
    # Ile Fahrenheitów to 20 st. C?

    print(convert_temperature_in_celsius_to_fahrenheit(20))

    # Zadanie 3 Napisz funkcję która zwraca objętość prostopadłościanu o bokach: a, b, c
    # Wykorzystując zdefiniowaną funkcję, policz objętość prostopadłościanu o wymiarach 3x5x7

    print(calculate_volume_of_the_cuboid(3, 5, 7))
