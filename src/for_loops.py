def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    for integer in range(1, 11):
        print(integer ** 2)


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "tosia", "niCKI", "STEPHANE"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
