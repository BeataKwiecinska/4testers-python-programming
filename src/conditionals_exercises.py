def display_speed_information(speed):
    if speed > 50:
        print('Slow down! :(')
    else:
        print('Thank you, your speed is below limit! :)')


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

