movies = ['Dune', 'Star Wars', 'Blade Runner', 'Stalker', 'Lost Highway']

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')
print(len(movies))
middle_movies = movies[2:6]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

# Zadanie Dla listy: emails = [‘a@example.com’, ‘b@example.com’] Wydrukuj: długość listy pierwszy element ostatni element
# Dodaj nowy email ‘cde@example.com’
emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')

# Zadanie Zdefiniuj zmienną friend zawierającą imię, wiek oraz listę dwóch hobby Twojego przyjaciela w postaci słownika.
friend = {
    "first_name": "Aldona",
    "age": 39,
    "hobby": ["cycling", "swimming"]
}
friend_hobbies = friend["hobby"]
print("Hobby of my freind:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"]. append("football")
print(friend)
friend["married"] = True
friend["age"] = 40
print(friend)
