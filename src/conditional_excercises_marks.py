def get_grade_ifo(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):
        return 'n/A'
    elif grade >= 4.5:
        return 'Bardzo dobry'
    elif grade >= 4.0:
        return 'dobry'
    elif grade >= 3.0:
        return 'dostateczny'
    else:
        return 'niedostateczny'

if __name__ == '__main__':
    print(get_grade_ifo(5))
    print(get_grade_ifo(4.5))
    print(get_grade_ifo(4))
    print(get_grade_ifo(3))
    print(get_grade_ifo(2))
    print(get_grade_ifo(1))
    print(get_grade_ifo(6))


