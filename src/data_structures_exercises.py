# Zadanie Napisz funkcję, która przyjmie argument w postaci listy liczb i zwróci ich sumę
# Napisz drugą funkcję, która przyjmie dwie liczby i zwróci ich średnią
# Z użyciem tych funkcji policz średnią temperaturę w ciągu dwóch miesięcy opisanych listami: january = [-4, 1.0, -7, 2]
# february= [-13, -9, -3, 3]


# Zadanie Utwórz funkcję generującą losowe dane do logowania. Argumentem jaki funkcja przyjmuje jestemail użytkownika
# (np. “user@example.com”)

import random
import string


def calculate_average_of_list_numbers(input_list):
    return sum(input_list)


def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


def generate_login_data_for_email(email):
    random_password = generate_random_password()
    return {'email': email, 'password': random_password}


def describe_player(player_dictionary):
    nick = player_dictionary["nick"]
    type = player_dictionary["type"]
    exp_points = player_dictionary["exp_points"]
    print(f"The player '{nick}' is of type {type.capitalize} and has {exp_points} EXP")


if __name__ == '__main__':
    january = [-4, 1.8, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

print(generate_login_data_for_email("adam@example.com"))
print(generate_login_data_for_email("kasia@example.com"))
print(generate_login_data_for_email("michal@example.com"))

# Zadanie Utwórz funkcję, która przyjmie słownik opisujący gracza:
# {“nick”: “maestro_54”,“type”: “warrior”,“exp_points”: 3000}i drukuje jego opis w postaci:
# “The player maestro_54 is of type warrior and has 3000 EXP”

player1 = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000,
}
player2 = {
    "nick": "wendy",
    "type": "master",
    "exp_points": 2000,
}
describe_player(player1)
describe_player(player2)
