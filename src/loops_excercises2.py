# Zadanie
# Stwórz funkcję przyjmującą listę temperatur (w st. Celsiusza) i
# zwracającą listę w Fahrenheitach
# Użyj jej dla listy
# temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]

def map_list_of_temperatures_from_celsius_to_farenheit(temps_celsius):
    temps_ferenheit = []
    for temp in temps_celsius:
        temp_f = temp * 9 / 5 + 32
        temps_ferenheit.append(temp_f)
    return temps_ferenheit


if __name__ == '__main__':
    temps_celsius = [11.0, 10.3, 69, 80, 43, 23, 1, 5, 77]
    print(map_list_of_temperatures_from_celsius_to_farenheit(temps_celsius))
