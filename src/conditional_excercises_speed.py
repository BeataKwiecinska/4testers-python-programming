def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_spssding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('You just lost your driving license :(')
    elif speed > 50:
        print(f'You just got a fine! Fine amount {calculate_fine_amount(speed)} :')
    else:
        print('Thank you, your speed is fine')


if __name__ == '__main__':
    print_the_value_of_spssding_fine_in_built_up_area(101)
    print_the_value_of_spssding_fine_in_built_up_area(100)
    print_the_value_of_spssding_fine_in_built_up_area(49)
    print_the_value_of_spssding_fine_in_built_up_area(51)
    print_the_value_of_spssding_fine_in_built_up_area(50)
