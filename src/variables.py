first_name = "Beata"
last_name = "Kwiecińska"
age = 39

print(first_name)
print(last_name)
print(age)

name = 'Aldona'
age_in_years = 39
number_of_animals = 1
driving_licence = True
friendship_duration_in_years = "20.5"

print(name, age_in_years, number_of_animals, driving_licence, friendship_duration_in_years)
print("name:", name, sep=',')
print(name, age_in_years, number_of_animals, driving_licence, friendship_duration_in_years, sep=',')
