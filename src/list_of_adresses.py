adresses = [
    {'city': "London", 'street': 'Elm', 'house_number': '23', 'postal_code': '55-456'},
    {'city': "Warsaw", 'street': 'Aleje_Jerozolimskie', 'house_number': '123', 'postal_code': '23-455'},
    {'city': "Cracow", 'street': 'Florianska', 'house_number': '22', 'postal_code': '23-456'},

]
if __name__ == '__main__':
    print(adresses[2]['postal_code'])
    print((adresses[1]['city']))
    adresses[0]['city'] = 'Vienna'
    print(adresses)
