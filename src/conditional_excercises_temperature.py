def check_normal_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False

if __name__ == '__main__':
    print(check_normal_conditions(0, 1013))
    print(check_normal_conditions(1, 1013))
    print(check_normal_conditions(0, 1014))
    print(check_normal_conditions(1, 1013))

